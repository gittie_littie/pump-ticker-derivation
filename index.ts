import WebSocket from "ws";
import axios from "axios";
// import * as crypto from "crypto";
import { createHash } from "sha256-uint8array";
import { toBigIntBE } from "bigint-buffer";


    export interface X {
        txIndexes: number[];
        nTx: number;
        totalBTCSent: number;
        estimatedBTCSent: number;
        reward: number;
        size: number;
        blockIndex: number;
        prevBlockIndex: number;
        height: number;
        hash: string;
        mrklRoot: string;
        version: number;
        time: number;
        bits: number;
        nonce: number;
    }

    export interface RootObject {
        op: string;
        x: X;
    }




function getInt64Bytes(x: number): Uint8Array {
	let y= Math.floor(x/2**32);
	return new Uint8Array([y,(y<<8),(y<<16),(y<<24), x,(x<<8),(x<<16),(x<<24)].map(z=> z>>>24))
  }
  


function concatenate(arrays: Uint8Array[]) {
	// Calculate byteSize from all arrays
	let size = arrays.reduce((a, b) => a + b.byteLength, 0);
	// Allcolate a new buffer
	let result = new Uint8Array(size);

	// Build the new array
	let offset = 0;
	for (let arr of arrays) {
		result.set(arr, offset);
		offset += arr.byteLength;
	}

	return result;
}



axios
	.get("https://pumps.fairpumps.io")
	.then((response: { data: { time: number, ticker: string[] } }) => {
		console.log(`We've received the following date from fairpumps: ${new Date(response.data.time * 1000)}!`);
		if (Date.now() / 1000 + 3600 < response.data.time) {
			console.log("There's at least an hour left until the pump starts! There's no reason to have this open yet!");
		} else if (Date.now() / 1000 + 3600 > response.data.time && response.data.time > Date.now() / 1000) {
			console.log(
				` There are roughly ${response.data.time - Math.round(Date.now() / 1000)} seconds left! We will start fetching data!`,
			);
			deriveTicker(response.data.time, response.data.ticker);
		} else {
			console.log("It looks like you missed the pump!");
		}
	})
	.catch(function (error: any) {
		// handle error
		console.log(error);
	});

const deriveTicker = (pumpTime: number, ticker: string[]) => {
	const ws = new WebSocket("wss://ws.blockchain.info/inv");
	ws.onopen = () => {
		ws.send(`{"op":"blocks_sub"}`);
	};
	ws.onmessage = (event: any) => {
		let tx = JSON.parse(event.data) as RootObject;

		if (tx.x.time >= pumpTime) { 
			const hash = createHash().update(concatenate([getInt64Bytes(tx.x.time), Buffer.from(tx.x.hash, 'hex')])).digest();
			console.log(ticker[Number(toBigIntBE(Buffer.from(hash)) % BigInt(ticker.length))]);
			ws.close();
		}

	};
};
